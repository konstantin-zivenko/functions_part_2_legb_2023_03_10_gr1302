from collections import Counter


txt = """Function call semantics are described in more detail in section Calls.
A function call always assigns values to all parameters
mentioned in the parameter list, either from positional arguments, from keyword
arguments, or from default values. If the form “*identifier”
is present, it is initialized to a tuple receiving any excess positional
parameters, defaulting to the empty tuple. If the form “**identifier”
is present, it is initialized to a new ordered mapping receiving any excess
keyword arguments, defaulting to a new empty mapping of the same type.
Parameters after “*” or “*identifier” are keyword-only parameters and may only be
passed by keyword arguments. Parameters before “/” are positional-only
parameters and may only be passed by positional arguments.
Changed in version 3.8: The / function parameter syntax may be used to indicate
positional-only parameters. See PEP 570 for details.
Parameters may have an annotation of the form “: expression” following the parameter
name. Any parameter may have an annotation, even those of the form
*identifier or **identifier. Functions may have “return” annotation of the form
“-> expression” after the parameter list.
These annotations can be any valid Python expression. The presence of annotations
does not change the semantics of a function. The annotation values are
available as values of a dictionary keyed by the parameters’ names in the
annotations attribute of the function object. If the annotations import
from future is used, annotations are preserved as strings at runtime which
enables postponed evaluation. Otherwise, they are evaluated when
the function definition is executed. In this case annotations may be evaluated in a
different order than they appear in the source code.
It is also possible to create anonymous functions (functions not bound to a name),
for immediate use in expressions. This uses lambda expressions,
described in section Lambdas. Note that the lambda expression is merely a shorthand
for a simplified function definition; a function defined in a “def”
statement can be passed around or assigned to another name just like a function
defined by a lambda expression. The “def” form is actually more powerful
since it allows the execution of multiple statements and annotations.
Programmer’s note: Functions are first-class objects. A “def” statement executed
inside a function definition defines a local function that can be returned
or passed around. Free variables used in the nested function can access the local
variables of the function containing the def.
See section Naming and binding for details."""

new_txt = ""
for item in txt:
    if item.isalpha() or item.isspace():
        new_txt += item
print(new_txt)
new_txt_splited = new_txt.split()
print(len(new_txt_splited))
count = Counter(new_txt_splited).most_common()
for k, v in count:
    print(k, " - ", v)
