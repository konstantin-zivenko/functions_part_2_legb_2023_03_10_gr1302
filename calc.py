def add(x, y):
    return x + y


def subtract(x, y):
    return x - y


def multiply(x, y):
    return x * y


def divide(x, y):
    if y == 0:
        raise ZeroDivisionError("message")
    else:
        return x / y


def power(x, y):
    return x**y


def square_root(x):
    return x**0, 5


def cube_root(x):
    return x ** (1 / 3)


print(
    """\nОберіть операцію:
1. Додавання
2. Віднімання
3. Множення
4. Ділення
5. Зведення в ступінь
6. Квадратний корінь
7. Кубічний корінь
8. Завершити програму"""
)

while True:
    choice = input("Ваш вибір: ")
    if choice in ("1", "2", "3", "4", "5"):
        x = float(input("Введіть перше число: "))
        y = float(input("Введіть друге число: "))

        if choice == "1":
            print(x, "+", y, "=", add(x, y))

        elif choice == "2":
            print(x, "-", y, "=", subtract(x, y))

        elif choice == "3":
            print(x, "*", y, "=", multiply(x, y))

        elif choice == "4":
            result = divide(x, y)
            if result is not None:
                print(x, "/", y, "=", result)

        elif choice == "5":
            print(x, "у ступені", y, "=", power(x, y))

    elif choice in ("6", "7"):
        x = float(input("Введіть число: "))

        if choice == "6":
            print("Квадратний корінь з", x, "=", square_root(x))

        else:
            print("Кубічний корінь з", x, "=", cube_root(x))

    elif choice == "8":
        break

    else:
        print("Невірний вибір!")
